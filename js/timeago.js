
/**
 * @file
 * Attaches the behaviors for the Timeago module.
 */

(function ($) {
  Drupal.behaviors.timeago = {
    attach: function (context, settings) {
      // Initialize the timestamps
      $('abbr.timeago').timeago();
    }
  };
}(jQuery));
