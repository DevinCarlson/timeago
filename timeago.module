<?php

/**
 * @file
 * Primarily Drupal hooks.
 */

/**
 * Implements hook_requirements().
 */
function timeago_requirements($phase) {
  // Create an array to hold Timeago requirements
  $requirements = array();

  // Check requirements during the runtime phase
  if ($phase == 'runtime') {
    // Check if the Timeago jQuery plugin library is installed
    if (($library = libraries_detect('timeago')) && !empty($library['installed'])) {
      $requirements['timeago_library'] = array(
        'title' => t('Timeago jQuery plugin'),
        'value' => t('Installed'),
        'severity' => REQUIREMENT_OK,
      );
    }
    else {
      $requirements['timeago_library'] = array(
        'title' => t('Timeago jQuery plugin'),
        'value' => t('Not installed'),
        'description' => $library['error message'],
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }

  return $requirements;
}

/**
 * Implements hook_libraries_info().
 */
function timeago_libraries_info() {
  $libraries['timeago'] = array(
    'name' => 'Timeago',
    'vendor url' => 'http://timeago.yarp.com',
    'download url' => 'http://timeago.yarp.com',
    'version' => '1.5',
    'files' => array(
      'js' => array(
        'jquery.timeago.js',
      ),
    ),
    'variants' => array(),
  );

  foreach (array('ar', 'bg','bs', 'ca', 'cy', 'cz', 'da', 'de', 'el', 'en-short', 'en', 'es', 'fa', 'fi', 'fr-short', 'fr', 'he', 'hr', 'hu', 'hy', 'id', 'it', 'ja', 'ko', 'lt', 'mk', 'nl', 'no', 'pl', 'pt-br', 'pt', 'ro', 'ru', 'sk', 'sl', 'sv', 'th', 'tr', 'uk', 'uz', 'zh-CN', 'zh-TW') as $langcode) {
    $libraries['timeago']['variants'][$langcode] = array(
      'files' => array(
        'js' => array(
          'jquery.timeago.js',
          'locales/jquery.timeago.' . $langcode . '.js',
        ),
      ),
    );
  }

  return $libraries;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function timeago_form_node_type_form_alter(&$form, &$form_state, $form_id) {
  $type = $form['#node_type'];

  $form['display']['timeago_node_submitted'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display date information using automatically updating fuzzy timestamps'),
    '#default_value' => variable_get('timeago_submitted_' . $type->type, TRUE),
    '#description' => t('Publish date will be displayed using automatically updating fuzzy timestamps (e.g. "4 minutes ago" or "about 1 day ago").'),
    '#states' => array(
      'visible' => array(
        ':input[name="node_submitted"]' => array('checked' => TRUE),
      ),
    ),
  );

  if (module_exists('comment')) {
    $form['comment']['timeago_comment_submitted'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display date information using automatically updating fuzzy timestamps'),
      '#default_value' => variable_get('timeago_node_submitted_' . $type->type, TRUE),
      '#description' => t('Publish date will be displayed using automatically updating fuzzy timestamps (e.g. "4 minutes ago" or "about 1 day ago").'),
    );
  }
}

/**
 * Converts a timestamp into a Timeago date.
 *
 * @param $timestamp
 *   A UNIX timestamp.
 * @param $text
 *   (Optional) A human-readable date (will be displayed if JS is disabled).
 *   If not provided, the site default date format is used.
 * @return
 *   HTML representing a Timeago-friendly date.
 */
function timeago_add_timeago($timestamp, $text = '') {
  // The fallback date isn't set, so we have to generate it ourselves.
  if (empty($text)) {
    $text = format_date($timestamp);
  }

  // Construct the Timeago element.
  $html = '<abbr class="timeago" title="' . date_iso8601($timestamp) . '">' . $text . '</abbr>';

  return $html;
}

/**
 * Implements hook_node_view().
 */
function timeago_node_view($node, $view_mode, $langcode) {
  if (variable_get('node_submitted_' . $node->type, TRUE)) {
    if (variable_get('timeago_node_submitted_' . $node->type, TRUE)) {
      $node->content['#attached']['libraries_load'][] = array('timeago', $langcode);
      $node->content['#attached']['js'][] = drupal_get_path('module', 'timeago') . '/js/timeago.js';
    }
  }
}

/**
 * Implements hook_comment_view().
 */
function timeago_comment_view($comment, $view_mode, $langcode) {
  $node = node_load($comment->nid);
  if (variable_get('timeago_comment_submitted_' . $node->type, TRUE)) {
    $comment->content['#attached']['libraries_load'][] = array('timeago', $langcode);
    $comment->content['#attached']['js'][] = drupal_get_path('module', 'timeago') . '/js/timeago.js';
  }
}

/**
 * Implements hook_process_node().
 */
function timeago_process_node(&$variables) {
  $node = $variables['node'];
  if (variable_get('node_submitted_' . $node->type, TRUE)) {
    if (variable_get('timeago_node_submitted_' . $node->type, TRUE)) {
      $variables['date'] = timeago_add_timeago($node->created, $node->created);

      $variables['submitted'] = t('Submitted by !username !datetime', array('!username' => $variables['name'], '!datetime' => $variables['date']));
    }
  }
}

/**
 * Implements hook_process_comment().
 */
function timeago_preprocess_comment(&$variables) {
  $node = $variables['node'];
  if (variable_get('timeago_comment_submitted_' . $node->type, TRUE)) {
    $comment = $variables['comment'];

    $variables['created'] = timeago_add_timeago($comment->created, $comment->created);

    // Avoid calling format_date() twice on the same timestamp.
    if ($comment->changed == $comment->created) {
      $variables['changed'] = $variables['created'];
    }
    else {
      $variables['changed'] = timeago_add_timeago($comment->created, $comment->changed);
    }

    $variables['submitted'] = t('Submitted by !username on !datetime', array('!username' => $variables['author'], '!datetime' => $variables['created']));
  }
}
